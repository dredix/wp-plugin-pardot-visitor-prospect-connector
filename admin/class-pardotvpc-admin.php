<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.dredix.net
 * @since      1.0.0
 *
 * @package    Pardotvpc
 * @subpackage Pardotvpc/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Pardotvpc
 * @subpackage Pardotvpc/admin
 * @author     Dredix Inc <dredix@dredix.net>
 */
class Pardotvpc_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * @var Pardot_Handler
	 */
	private $pardotApiHandler;

	/**
	 * @var string
	 */
	private $emailValue = '';


	private $wpdb;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param string $plugin_name The name of this plugin.
	 * @param string $version The version of this plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct( $plugin_name, $version ) {
		global $wpdb;
		$this->wpdb = $wpdb;

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

		add_action( 'wpcf7_mail_sent', [ &$this, 'action_wpcf7_mail_sent' ], 10, 1 );

		if ( ! empty( $this->get_setting( 'post_process_hook' ) ) ) {
			$hook = $this->get_setting( 'post_process_hook' );
			add_filter( $hook, array( &$this, 'process_post' ), 10, 2 );
		}
		if ( ! empty( $this->get_setting( 'hook' ) ) ) {
			$hook = $this->get_setting( 'hook' );
			add_filter( $hook, array( &$this, 'process_filter' ), 10, 4 );
		}

	}

	/**
	 * Handles hook action for when a Contact Form 7 form is submitted
	 *
	 * @param $contact_form
	 *
	 * @return bool
	 */
	function action_wpcf7_mail_sent( $contact_form ) {
		$processed = false;
		if ( isset( $_POST['_wpcf7'] ) ) {
			$postForm = $_POST['_wpcf7'];
			$mappings = $this->get_mapping_value( true );
			foreach ( $mappings as $formIndex => $mapping ) {
				if ( $mapping->form == $postForm ) {
					$data2Post = [];
					foreach ( $mapping->mappings as $mappingFields ) {
						$value = ! empty( $mappingFields->form_field ) && isset( $_POST[ $mappingFields->form_field ] )
							? $_POST[ $mappingFields->form_field ]
							: $mappingFields->default;

						$data2Post[ $mappingFields->pardot_field ] = $value;
					}

					if ( isset( $data2Post['email'] ) && $prospect = $this->pardotApiHandler->createProspect( $data2Post ) ) {
						$processed = $this->assign_prospect_to_visitor( $prospect->id,
							$this->pardotApiHandler->getVisitorId() );
					} else {
						$this->log(
							"Form not submitted because not email field in field mapping",
							'warning',
							[
								'formId'        => $postForm,
								'formIndex'     => $formIndex + 1,
								'mapping'       => $mapping,
								'submittedData' => $_POST
							]
						);
					}
				}
			}
		}

		return $processed;
	}

	/**
	 * Gets the form and field mappings
	 *
	 * @param bool $convertJson Should the out be a PHP array
	 *
	 * @return array|string
	 */
	public function get_mapping_value( $convertJson = false ) {
		$mappingsValue = '[]';
		try {
			$mappingsValueRaw = $this->get_setting( 'mappings' );
			if ( ! empty( $mappingsValue ) ) {
				$mappingsValue = base64_decode( $mappingsValueRaw );
			}
			if ( empty( $mappingsValue ) ) {
				$mappingsValue = '[]';
			}
		} catch ( Exception $e ) {
			echo "<div class='error'>" . $e->getMessage() . "</div>";
			$mappingsValue = '[]';
		}

		return $convertJson ? json_decode( $mappingsValue ) : $mappingsValue;
	}

	/**
	 * @return Pardot_Handler
	 */
	public function get_pardot_api_handler() {
		return $this->pardotApiHandler;
	}

	/**
	 * @param Pardot_Handler $pardotApiHandler
	 *
	 * @return Pardotvpc_Admin
	 */
	public function set_pardot_api_handler( $pardotApiHandler ) {
		$this->pardotApiHandler = $pardotApiHandler;

		return $this;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/pardotvpc-admin.css', array(),
			$this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/pardotvpc-admin.js', array( 'jquery' ),
			$this->version, false );
	}

	public function admin_menu_action() {
		$page_title = 'Pardot Visitor Prospect Connector';
		$menu_title = 'Pardot VP Connector';
		$capability = 'manage_options';
		$menu_slug  = 'pardot-vp-connector';
		$function   = [ $this, 'pardot_vp_connector_settings_page' ];
		$icon_url   = 'dashicons-email';
		$position   = 40;
		add_submenu_page( 'wpcf7', $page_title, $menu_title, $capability, $menu_slug, $function );
	}


	public function pardot_vp_connector_settings_page() {
		if ( isset( $_POST['action'] ) && $_POST['action'] == 'update' ) {
			$this
				->update_options( 'username', $_REQUEST['pardotvpcs_username'] )
				->update_options( 'password', $_REQUEST['pardotvpcs_password'] )
				->update_options( 'api_user_key', $_REQUEST['pardotvpcs_api_user_key'] )
				->update_options( 'post_process_hook', $_REQUEST['pardotvpcs_post_process_hook'] )
				->update_options( 'hook', $_REQUEST['pardotvpcs_hook'] )
				->update_options( 'email_field', $_REQUEST['pardotvpcs_email_field'] )
				->update_options( 'mappings', $_REQUEST['pardotvpcs_mappings'] );
			$this->log( 'Settings updated by user: ' . get_current_user_id(), 'info', null );
			header( "Location: " . $_REQUEST['_wp_http_referer'] . '&saved=1' );
			die();
		} elseif ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'delete_log' ) {
			$this->delete_log_file();
			header( "Location: " . $_SERVER['HTTP_REFERER'] );
			die;
		}

		$formsDb = $this->wpdb->get_results( 'SELECT * FROM ' . $this->wpdb->prefix . 'posts WHERE post_type = "wpcf7_contact_form"' );
		$forms   = [];
		foreach ( $formsDb as $formDb ) {
			if ( ! empty( $formDb->post_title ) ) {
				$forms[] = [
					'id'    => $formDb->ID,
					'title' => $formDb->post_title,
				];
			}
		}

		$nowPath  = __DIR__;
		$viewFile = $nowPath . '/views/settings.php';
		require( $viewFile );
	}

	public function get_setting( $key ) {
		$key = sprintf( "%s_%s", PARDOTVPC_SLUG, $key );

		return get_option( $key );
	}

	public function update_options( $key, $value ) {
		$key = sprintf( "%s_%s", PARDOTVPC_SLUG, $key );
		update_option( $key, $value );

		return $this;
	}

	/**
	 * Returns the current pardot visitor ID
	 * @return int|null
	 */
	public function get_current_visitor_id() {
		foreach ( $_COOKIE as $key => $value ) {
			if ( strpos( $key, 'visitor_id' ) !== false ) {
				return $value;
			}
		}

		return null;
	}

	/**
	 * Used to write a message to the log file
	 *
	 * @param string $message
	 * @param string $type
	 * @param array $context
	 *
	 * @return $this
	 */
	public function log( $message, $type = 'debug', $context = array() ) {
		$log = [
			'datetime' => date( 'Y-m-d H:i:s' ),
			'message'  => $message,
			'type'     => $type,
			'context'  => $context
		];

		file_put_contents(
			PARDOTVPC_LOG_PATH,
			json_encode( $log ) . PHP_EOL,
			FILE_APPEND
		);

		return $this;
	}

	/**
	 * Used process the the action of linking the visitor to the prospect after the data has been posted
	 * @return bool
	 */
	public function process_post() {
		$prospectId = $this->pardotApiHandler->getProspectIdByEmail( $this->emailValue );
		$visitorId  = $this->pardotApiHandler->getVisitorId();

		return $this->assign_prospect_to_visitor( $prospectId, $visitorId );
	}

	/**
	 * Used to call the method used to assign a prospect to a visitor
	 *
	 * @param $prospectId
	 * @param $visitorId
	 *
	 * @return bool
	 */
	protected function assign_prospect_to_visitor( $prospectId, $visitorId ) {
		if ( ! empty( $visitorId ) && ! empty( $prospectId ) ) {
			$assigned = $this->pardotApiHandler->assignProspectToVisitor( $visitorId, $prospectId );
			$this->log( 'Assigning visitor to prospect', 'info', compact( 'prospectId', 'visitorId', 'assigned' ) );
		}

		return true;
	}

	/**
	 * Used to retrieve the email address from the filter when processing the data to be posted
	 * @return array|bool
	 */
	public function process_filter() {
		$contextData = func_get_args();
		$emailField  = $this->get_setting( 'email_field' );
		$mappings    = [];
		try {
			$mappings = $this->get_setting( 'mappings' );
			$mappings = json_decode( base64_decode( $mappings ) );
		} catch ( Exception $e ) {
			$this->log(
				"Error while attempting retrieve and convert mappings in 'process_filter'. Error details: " . $e->getMessage(),
				'error',
				compact( 'mappings' )
			);
		}

		//If no mapping, then user process_post which happens after form is posted using 3rd party plugin
		if ( count( $mappings ) > 0 ) {
			$submitData = [];
			foreach ( $mappings as $mapping ) {
				$submitData[ $mapping->pardot_field ] = $contextData[0][ $mapping->form_field ];
			}

			if ( $prospect = $this->pardotApiHandler->createProspect( $submitData ) ) {
				$this->assign_prospect_to_visitor( $prospect->id, $this->pardotApiHandler->getVisitorId() );

				return true;
			}
		}

		$this->emailValue = $contextData[0][ $emailField ];     //Getting the email so this can be used after the data POST

		return $contextData[0];
	}

	/**
	 * Used to retrieve x lines from the log file
	 *
	 * @param string $filepath
	 * @param int $lines
	 * @param int $skip
	 * @param bool $adaptive
	 *
	 * @return bool|string
	 */
	function tail_with_skip( $filepath, $lines = 1, $skip = 0, $adaptive = true ) {
		// Open file
		$f = @fopen( $filepath, "rb" );
		if ( @flock( $f, LOCK_SH ) === false ) {
			return false;
		}
		if ( $f === false ) {
			return false;
		}

		if ( ! $adaptive ) {
			$buffer = 4096;
		} else {
			// Sets buffer size, according to the number of lines to retrieve.
			// This gives a performance boost when reading a few lines from the file.
			$max    = max( $lines, $skip );
			$buffer = ( $max < 2 ? 64 : ( $max < 10 ? 512 : 4096 ) );
		}

		// Jump to last character
		fseek( $f, - 1, SEEK_END );

		// Read it and adjust line number if necessary
		// (Otherwise the result would be wrong if file doesn't end with a blank line)
		if ( fread( $f, 1 ) == "\n" ) {
			if ( $skip > 0 ) {
				$skip ++;
				$lines --;
			}
		} else {
			$lines --;
		}

		// Start reading
		$output = '';
		$chunk  = '';
		// While we would like more
		while ( ftell( $f ) > 0 && $lines >= 0 ) {
			// Figure out how far back we should jump
			$seek = min( ftell( $f ), $buffer );

			// Do the jump (backwards, relative to where we are)
			fseek( $f, - $seek, SEEK_CUR );

			// Read a chunk
			$chunk = fread( $f, $seek );

			// Calculate chunk parameters
			$count  = substr_count( $chunk, "\n" );
			$strlen = mb_strlen( $chunk, '8bit' );

			// Move the file pointer
			fseek( $f, - $strlen, SEEK_CUR );

			if ( $skip > 0 ) { // There are some lines to skip
				if ( $skip > $count ) {
					$skip  -= $count;
					$chunk = '';
				} // Chunk contains less new line symbols than
				else {
					$pos = 0;

					while ( $skip > 0 ) {
						if ( $pos > 0 ) {
							$offset = $pos - $strlen - 1;
						} // Calculate the offset - NEGATIVE position of last new line symbol
						else {
							$offset = 0;
						} // First search (without offset)

						$pos = strrpos( $chunk, "\n", $offset ); // Search for last (including offset) new line symbol

						if ( $pos !== false ) {
							$skip --;
						} // Found new line symbol - skip the line
						else {
							break;
						} // "else break;" - Protection against infinite loop (just in case)
					}
					$chunk = substr( $chunk, 0, $pos ); // Truncated chunk
					$count = substr_count( $chunk, "\n" ); // Count new line symbols in truncated chunk
				}
			}

			if ( strlen( $chunk ) > 0 ) {
				// Add chunk to the output
				$output = $chunk . $output;
				// Decrease our line counter
				$lines -= $count;
			}
		}

		// While we have too many lines
		// (Because of buffer size we might have read too many)
		while ( $lines ++ < 0 ) {
			// Find first newline and remove all text before that
			$output = substr( $output, strpos( $output, "\n" ) + 1 );
		}

		// Close file and return
		@flock( $f, LOCK_UN );
		fclose( $f );

		return trim( $output );
	}

	/**
	 * Deletes the log file
	 */
	public function delete_log_file() {
		if ( file_exists( PARDOTVPC_LOG_PATH ) ) {
			unlink( PARDOTVPC_LOG_PATH );
			sleep( 1 );
			$this->log( 'Log file deleted by user: ' . get_current_user_id(), 'info' );
		}
	}

	public function add_action_links( $links ) {
		$links[] = sprintf(
			'<a href="%s">%s</a>',
			admin_url( 'admin.php?page=pardot-vp-connector' ),
			___( 'Settings' )
		);

		return $links;
	}
}
