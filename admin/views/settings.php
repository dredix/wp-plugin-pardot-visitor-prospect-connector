<?php
/**
 * @var Pardotvpc_Admin $this
 * @var array $form
 */

$fileDataArray = [];
if ( file_exists( PARDOTVPC_LOG_PATH ) ) {
	$fileDataRaw = $this->tail_with_skip( PARDOTVPC_LOG_PATH, 200 );
	$fileData    = explode( "\n", $fileDataRaw );

	foreach ( $fileData as $fileLine ) {
		$fileDataArray[] = json_decode( $fileLine );
	}
}

//Getting form mapping values
$mappingsValue = $this->get_mapping_value();

?>
<script src="<?php echo PARDOTVPC_PATH . '/admin/js/vue/dist/vue.js' ?>"></script>
<script src="<?php echo PARDOTVPC_PATH . '/admin/js/vuejs-datatable.js' ?>"></script>

<div class='wrap' id="pardotvpcApp">
    <div class="page-header">
        <img src="http://wordpress.local/wp-content/plugins/pardot/images/pardot-logo.png"
             alt="Pardot, a Salesforce Company" width="181" height="71" class="alignleft_"/>
        <h1><?php echo PARDOTVPC_NAME ?> Settings</h1>
    </div>

    <div class="notifications">

		<?php if ( isset( $_REQUEST['saved'] ) && $_REQUEST['saved'] == '1' ) { ?>
            <div class="updated notice">
                <p>Awesome, your settings have been updated.</p>
            </div>
		<?php } ?>

        <div class="updated notice">
            <p>Use the form below to update your details related to Pardot.</p>
            <p>These will be used to connect to the v4 API and assigning a visitor to a prospect by using the data
                submitted form a Contact From 7.</p>
        </div>
    </div>
    <br/>
    <br/>
    <form method="post">     <?php settings_fields( 'extra-post-info-settings' ); ?>     <?php do_settings_sections( 'extra-post-info-settings' ); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Email:</th>
                <td><input
                            type="text"
                            required
                            name="<?php echo PARDOTVPC_KEY_USERNAME ?>"
                            value="<?php echo $this->get_setting( 'username' ); ?>"
                    />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Password:</th>
                <td><input
                            type="password"
                            required
                            name="<?php echo PARDOTVPC_KEY_PASSWORD ?>"
                            value="<?php echo $this->get_setting( 'password' ); ?>"
                    />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">API User Key:</th>
                <td>
                    <input
                            type="text"
                            required
                            name="<?php echo PARDOTVPC_KEY_USER_API_KEY ?>"
                            value="<?php echo $this->get_setting( 'api_user_key' ); ?>"
                    />
                    <p>Find your <em>"User Key"</em> in the <em>"My Profile"</em> section of your <a
                                href="https://pi.pardot.com/account/user" target="_blank">Pardot Account Settings</a>.
                    </p>
                </td>
            </tr>

        </table>
        <textarea name="<?php echo PARDOTVPC_KEY_MAPPINGS ?>" v-model="mapping_string" class="hidden"></textarea>

        <p>Ensure each mapping has an email field. If none is set, the data will not be submitted to Pardot.</p>

        <h2>Mapping</h2>
        <a class="button button-primary" v-on:click="addMappingSet">Add Form Mapping Set</a>
        <div class="field-mappings" v-for="mset in mapping_set">
            <div class="form-group">
                <label>Contact Form 7 Form</label>
                <select v-model="mset.form" required>
                    <option v-for="cf7_form in cf7_forms" :value="cf7_form.id">{{cf7_form.title}}</option>
                </select>
                <a class="button button-primary" v-on:click="removeMappingSet(mset)">Remove</a>
            </div>
            <table>
                <thead>
                <tr>
                    <th>Pardot Field</th>
                    <th>CF7 Form field</th>
                    <th>Default Value</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="mapping in mset.mappings">
                    <td>
                        <input type="text" v-model="mapping.pardot_field" placeholder="Pardot field" required
                               list="pardor_fields"/>
                    </td>
                    <td>
                        <input type="text" v-model="mapping.form_field" placeholder="Contact Form 7 field"/>
                    </td>
                    <td>
                        <input type="text" v-model="mapping.default" placeholder="Default value"/>
                    </td>
                    <td>
                        <a class="button button-primary" v-on:click="addMapping(mset)">Add </a>
                        <a class="button .button-secondary" title="Remove mapping"
                           v-on:click="removeMapping(mset, mapping)">Delete</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <datalist id="pardor_fields">
            <option v-for="option in prospect_fields" :value="option"/>
        </datalist>

		<?php submit_button(); ?>
    </form>
    <hr>
    <h2>Info</h2>
    <div>
        <p><span>Current visitor key:</span> <span><?php echo $this->get_current_visitor_id() ?></span></p>
    </div>
    <h3>Log Data</h3>
    <a
            href="<?php echo admin_url( 'admin.php?page=pardot-vp-connector&action=download' ) ?>"
            target="_blank"
            class="button button-primary"
            title="Download the log file with all log details."
    >
        Download Log File
    </a>

    <a
            href="<?php echo admin_url( 'admin.php?page=pardot-vp-connector&action=delete_log' ) ?>"
            class="button"
            title="Delete the log file to start over with fresh log data"
    >
        Delete Log File
    </a>

    <div class="row">
        <div class="col-xs-12 form-inline">
            <div class="form-group">
                <input type="text" class="form-control" v-model="filter" placeholder="Filter Log Table">
            </div>
        </div>
    </div>
    <div class="row">
        <div id="table" class="col-xs-12 table-responsive">
            <datatable :columns="columns" :data="rows" :filter-by="filter"></datatable>
        </div>
    </div>
</div>

<script>
    var pardotvpcApp = new Vue({
        el: '#pardotvpcApp',
        data: {
            filter: '',
            message: 'Hello Vue!',
            columns: [
                {label: 'Date Time', field: 'datetime', filterable: true},
                {label: 'Message', field: 'message', filterable: true},
                {label: 'Type', field: 'type', filterable: true},
                {
                    label: 'Context',
                    representedAs: function (row) {
                        return JSON.stringify(row.context);
                    },
                    sortable: false
                }
            ],
            rows: <?php echo json_encode( $fileDataArray ); ?>,
            //mappings: <?php //echo $mappingsValue ?>//,
            mapping_set: <?php echo $mappingsValue ?>,
            cf7_forms: <?php echo json_encode( $forms ) ?>,
            'prospect_fields': [
                'first_name',
                'last_name',
                'email',
                'phone',
                'fax',
                'company',
                'website',
                'job_title',
                'department',
                'address_one',
                'address_two',
                'city',
                'state',
                'territory',
                'zip',
                'country',
            ]
        },
        computed: {
            mapping_string: function () {
                // Base 64 encode the json string because WP is messing up the JSON string
                return btoa(JSON.stringify(this.mapping_set));
            }
        },
        methods: {
            addMapping: function (mset) {
                // Adds an empty mapping to the mapping array
                mset.mappings.push(
                    {
                        pardot_field: '',
                        form_field: '',
                        default: ''
                    }
                );
            },
            addMappingSet: function () {
                let mset = {
                    form: '',
                    mappings: []
                };
                this.addMapping(mset);
                this.mapping_set.push(mset);
            },
            removeMapping: function (mset, mapping) {
                // Remove a mapping line
                mset.mappings.splice(mset.mappings.indexOf(mapping), 1);
            },
            removeMappingSet: function (mset) {
                this.mapping_set.splice(this.mapping_set.indexOf(mset), 1);
            }
        }
    })
</script>