<?php

/**
 * Class Pardot_Handler
 * A class used to handle API call to the pardot API within WordPress
 */
class Pardot_Handler {
	const API_BASE_URL = 'https://pi.pardot.com/api';

	/** @var String */
	private $username;

	/** @var String */
	private $password;

	/** @var String */
	private $userKey;

	/** @var String */
	private $apiVersion;

	/** @var String */
	private $apiKey;

	/** @var string */
	private $logger = '';

	/**
	 * Pardot_Handler constructor.
	 *
	 * @param String $username
	 * @param String $password
	 * @param String $userKey
	 */
	public function __construct( $username, $password, $userKey ) {
		$this->username   = $username;
		$this->password   = $password;
		$this->userKey    = $userKey;
		$this->apiVersion = 4;
	}

	/**
	 * @return String
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * @param string $username
	 *
	 * @return Pardot_Handler
	 */
	public function setUsername( $username ) {
		$this->username = $username;

		return $this;
	}

	/**
	 * @return String
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param String $password
	 *
	 * @return Pardot_Handler
	 */
	public function setPassword( $password ) {
		$this->password = $password;

		return $this;
	}

	/**
	 * @return String
	 */
	public function getUserKey() {
		return $this->userKey;
	}

	/**
	 * @param String $userKey
	 *
	 * @return Pardot_Handler
	 */
	public function setUserKey( $userKey ) {
		$this->userKey = $userKey;

		return $this;
	}

	/**
	 * @return String
	 */
	public function getApiVersion() {
		return $this->apiVersion;
	}

	/**
	 * @param String $apiVersion
	 *
	 * @return Pardot_Handler
	 */
	public function setApiVersion( $apiVersion ) {
		$this->apiVersion = $apiVersion;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getLogger() {
		return $this->logger;
	}

	/**
	 * @param string $logger
	 *
	 * @return Pardot_Handler
	 */
	public function setLogger( $logger ) {
		$this->logger = $logger;

		return $this;
	}

	/**
	 * Gets the API key used to make restful request the thr pardot API
	 * @return string|null
	 */
	public function getApiKey() {
		if ( ! $this->apiKey ) {
			$url      = self::API_BASE_URL . "/login/version/4?format=json";
			$postData = [
				'email'    => $this->getUsername(),
				'password' => $this->getPassword(),
				'user_key' => $this->getUserKey(),
			];
			$response = wp_remote_post(
				$url,
				[
					'headers' => array(),
					'body'    => $postData,
				]
			);
			$body     = json_decode( wp_remote_retrieve_body( $response ) );

			if ( isset( $body->err ) ) {
				return null;
			} else {
				$this->apiKey = $body->api_key;
			}
		}

		return $this->apiKey;
	}

	/**
	 * Returns an header array used to make authenticated API call
	 * @return array
	 */
	protected function getRequestHeaders() {
		return [
			'Authorization' => sprintf(
				'Pardot api_key=%s, user_key=%s',
				$this->getApiKey(),
				$this->getUserKey()
			)
		];
	}

	/**
	 * Used to retrieve the Prospect ID base in the email address
	 *
	 * @param string $email
	 *
	 * @return int|null
	 */
	public function getProspectIdByEmail( $email ) {
		$url      = self::API_BASE_URL . "/prospect/version/{$this->getApiVersion()}/do/query?format=json&updated_after=today";
		$response = wp_remote_get(
			$url,
			[
				'headers' => $this->getRequestHeaders(),
				'cookies' => []
			]
		);
		$body     = json_decode( wp_remote_retrieve_body( $response ) );

		if ( isset( $body->result ) ) {
			$prospectData = is_array( $body->result->prospect ) ? $body->result->prospect : [ $body->result->prospect ];
			foreach ( $prospectData as $prospect ) {
				if ( $prospect->email == $email ) {
					return $prospect->id;
				}
			}
		}

		return null;
	}

	/**
	 *  Used to create a prospect using the pardot API
	 *
	 * @param array $prospectData
	 *
	 * @return object|null
	 */
	public function createProspect( $prospectData ) {
		$url      = self::API_BASE_URL . "/prospect/version/{$this->getApiVersion()}/do/create/email/" . $prospectData['email'];
		$response = wp_remote_get(
			sprintf( "%s?format=json&%s", $url, http_build_query( $prospectData ) ),
			[
				'headers' => $this->getRequestHeaders(),
				'cookies' => []
			]
		);
		try {
			$body = json_decode( wp_remote_retrieve_body( $response ) );

			return $body->prospect ? $body->prospect : null;
		} catch ( Exception $e ) {
			$this->log(
				'There was an error while creating the prospect via the pardot API. Error details: ' . $e->getMessage(),
				'error',
				compact( 'url', 'body' )
			);
		}

		return null;
	}

	/**
	 * Used to assign a visit to a prospect based on the IDs
	 *
	 * @param string $visitorId Visitor ID
	 * @param string $prospectId Prospect ID
	 *
	 * @return bool
	 */
	public function assignProspectToVisitor( $visitorId, $prospectId ) {
		$url      = self::API_BASE_URL . sprintf(
				"/visitor/version/%d/do/assign/id/%s?prospect_id=%s&format=json",
				$this->apiVersion,
				$visitorId,
				$prospectId
			);
		$response = wp_remote_get(
			$url,
			[
				'headers' => $this->getRequestHeaders(),
				'cookies' => []
			]
		);
		$body     = json_decode( wp_remote_retrieve_body( $response ) );
		if ( isset( $body->visitor ) && $body->visitor->id == $visitorId && $body->visitor->prospect_id == $prospectId ) {
			return true;
		}

		$this->log( 'Unable to assign visitor as prospect', 'info', $body->visitor );

		return false;
	}

	/**
	 * Getting the current user's visitor Id
	 * @return int|null
	 */
	public function getVisitorId() {
		foreach ( $_COOKIE as $key => $value ) {
			if ( strpos( $key, 'visitor_id' ) !== false ) {
				return $value;
			}
		}

		return null;
	}

	/**
	 * Used to write to a log file
	 *
	 * @param string $message
	 * @param string $type
	 * @param array $context
	 *
	 * @return $this
	 */
	private function log( $message, $type = 'debug', $context = array() ) {
		if ( $this->logger ) {
			$log = [
				'datetime' => date( 'Y-m-d H:i:s' ),
				'message'  => $message,
				'type'     => $type,
				'context'  => $context
			];

			file_put_contents(
				$this->logger,
				json_encode( $log ) . PHP_EOL,
				FILE_APPEND
			);

			return $this;
		}
	}
}