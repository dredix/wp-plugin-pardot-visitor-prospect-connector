<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.dredix.net
 * @since      1.0.0
 *
 * @package    Pardotvpc
 * @subpackage Pardotvpc/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pardotvpc
 * @subpackage Pardotvpc/includes
 * @author     Dredix Inc <dredix@dredix.net>
 */
class Pardotvpc_Activator {
	public static function activate() {
		if ( empty( get_option( PARDOTVPC_KEY_USERNAME ) ) ) {
			add_option( PARDOTVPC_KEY_USERNAME, '' );
			add_option( PARDOTVPC_KEY_PASSWORD, '' );
			add_option( PARDOTVPC_KEY_USER_API_KEY, '' );
		}
	}
}
