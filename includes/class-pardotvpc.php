<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.dredix.net
 * @since      1.0.0
 *
 * @package    Pardotvpc
 * @subpackage Pardotvpc/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Pardotvpc
 * @subpackage Pardotvpc/includes
 * @author     Dredix Inc <dredix@dredix.net>
 */
class Pardotvpc {
	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Pardotvpc_Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $plugin_name The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $version The current version of the plugin.
	 */
	protected $version;

	private $should_download_log = false;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct( $should_download_log = false ) {
		$this->should_download_log = $should_download_log;

		if ( defined( 'PARDOTVPC_VERSION' ) ) {
			$this->version = PARDOTVPC_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'pardotvpc';

		if ( ! $should_download_log ) {
			$this->load_dependencies();
			$this->set_locale();
			$this->define_admin_hooks();
		}
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Pardotvpc_Loader. Orchestrates the hooks of the plugin.
	 * - Pardotvpc_i18n. Defines internationalization functionality.
	 * - Pardotvpc_Admin. Defines all hooks for the admin area.
	 * - Pardotvpc_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pardotvpc-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pardotvpc-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-pardotvpc-admin.php';

		$this->loader = new Pardotvpc_Loader();
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Pardotvpc_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Pardotvpc_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		$pardorHandler = new Pardot_Handler(
			get_option( PARDOTVPC_KEY_USERNAME ),
			get_option( PARDOTVPC_KEY_PASSWORD ),
			get_option( PARDOTVPC_KEY_USER_API_KEY )
		);
		$pardorHandler->setLogger( PARDOTVPC_LOG_PATH );
		$plugin_admin = new Pardotvpc_Admin( $this->get_plugin_name(), $this->get_version() );

		$plugin_admin->set_pardot_api_handler( $pardorHandler );

		$this->loader->add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), $this, 'add_action_links' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'admin_menu_action' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		if ( $this->should_download_log ) {
			$this->download_log_handle();
		}
		$this->loader->run();
	}

	/**
	 * Triggers download of log file
	 */
	function download_log_handle() {
		$date = date( 'Y-m-d' );
		header( 'Content-Type: application/download' );
		header( sprintf( 'Content-Disposition: attachment; filename="debug.%s.txt"', $date ) );
		header( "Content-Length: " . filesize( PARDOTVPC_LOG_PATH ) );

		$fp = fopen( PARDOTVPC_LOG_PATH, "r" );
		fpassthru( $fp );
		fclose( $fp );
		exit;
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @return    string    The name of the plugin.
	 * @since     1.0.0
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @return    Pardotvpc_Loader    Orchestrates the hooks of the plugin.
	 * @since     1.0.0
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @return    string    The version number of the plugin.
	 * @since     1.0.0
	 */
	public function get_version() {
		return $this->version;
	}
}
