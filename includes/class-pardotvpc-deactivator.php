<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.dredix.net
 * @since      1.0.0
 *
 * @package    Pardotvpc
 * @subpackage Pardotvpc/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pardotvpc
 * @subpackage Pardotvpc/includes
 * @author     Dredix Inc <dredix@dredix.net>
 */
class Pardotvpc_Deactivator {

	public static function deactivate() {
		delete_option( PARDOTVPC_KEY_MAPPINGS );
	}
}
