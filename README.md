# Pardot Visitor Prospect Connector

This is a WordPress plugin which will link a Pardot visitor tracking cookie to a Contact form 7 submission.

## Description

This is a WordPress plugin which will link a Pardot visitor tracking cookie to a form submission. The UI allows you to link multiple Contact Form 7 forms to a Prospect.
You are able to map the field from Contact form 7 to the fields in the Pardot Prospect record so even custom field data can be filled from you contact form 7 submission

## Installation 

1. Upload and extract `pardotvpc.zip` (or whatever the filename ou download) to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Update the settings for the plugin which can be found under the Contact menu (for Contact Form 7)

## Plugin Info

* Donate link: http://www.dredix.net
* Tags: pardot, form submission, contact form 7
* Requires at least: 3.0.1
* Tested up to: 3.4
* Stable tag: v1.0.6
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html