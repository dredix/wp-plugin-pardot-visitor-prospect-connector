<?php
/**
 * @link              http://www.dredix.net
 * @since             1.0.0
 * @package           Pardotvpc
 *
 * @wordpress-plugin
 * Plugin Name:       Pardot Visitor Prospect Connector
 * Plugin URI:        https://bitbucket.org/dredix/wp-plugin-pardot-visitor-prospect-connector
 * Description:       This plugin is used to connect a visitor to a prospect in SafeForce Pardot after a Contact form 7 is submitted.
 * Version:           1.0.6
 * Author:            Dredix Inc
 * Author URI:        http://www.dredix.net
 * Developer URI:     http://www.dredix.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pardotvpc
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PARDOTVPC_VERSION', '1.0.6' );
define( 'PARDOTVPC_SLUG', 'pardotvpcs' );
define( 'PARDOTVPC_NAME', 'Pardot Visitor Prospect Connector' );
define( 'PARDOTVPC_PATH', plugin_dir_url( __FILE__ ) );
define( 'PARDOTVPC_LOG_PATH', __DIR__ . '/logs/debug.log' );
define( 'PARDOTVPC_API_BASE_ULR', 'https://pi.pardot.com/api' );

define( 'PARDOTVPC_KEY_USERNAME', sprintf( "%s_%s", PARDOTVPC_SLUG, 'username' ) );
define( 'PARDOTVPC_KEY_PASSWORD', sprintf( "%s_%s", PARDOTVPC_SLUG, 'password' ) );
define( 'PARDOTVPC_KEY_USER_API_KEY', sprintf( "%s_%s", PARDOTVPC_SLUG, 'api_user_key' ) );
define( 'PARDOTVPC_KEY_MAPPINGS', sprintf( "%s_%s", PARDOTVPC_SLUG, 'mappings' ) );

function activate_pardotvpc() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pardotvpc-activator.php';
	Pardotvpc_Activator::activate();
}

function deactivate_pardotvpc() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pardotvpc-deactivator.php';
	Pardotvpc_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pardotvpc' );
register_deactivation_hook( __FILE__, 'deactivate_pardotvpc' );

require plugin_dir_path( __FILE__ ) . 'includes/class-pardotvpc.php';
require plugin_dir_path( __FILE__ ) . 'includes/Pardot_Handler.php';

function run_pardotvpc() {
	$download_log = ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'download' );

	$plugin = new Pardotvpc( $download_log );
	$plugin->run();
}

run_pardotvpc();
