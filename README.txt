=== Plugin Name ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: http://www.dredix.net
Tags: pardot, form submission, contact form 7
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: v1.0.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This is a WordPress plugin which will link a Pardot visitor tracking cookie to a Contact form 7 submission.

== Description ==

This is a WordPress plugin which will link a Pardot visitor tracking cookie to a form submission. The UI allows you to link multiple Contact Form 7 forms to a Prospect.
You are able to map the field from Contact form 7 to the fields in the Pardot Prospect record so even custom field data can be filled from you contact form 7 submission

== Installation =

1. Upload and extract `pardotvpc.zip` (or whatever the filename ou download) to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Update the settings for the plugin which can be found under the Contact menu (for Contact Form 7)

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= v1.0.6 =
* Added the ability to map multiple form instead of just one

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`